//
//  File.swift
//  pokedex
//
//  Created by Jigar Panchal on 3/15/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co/"
let URL_POKEMON = "api/v1/pokemon/"

typealias DownloadComplete = () -> ()
