//
//  MoveCell.swift
//  pokedex
//
//  Created by Jigar Panchal on 3/22/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import UIKit

class MoveCell: UITableViewCell {
    @IBOutlet weak var typeLbl: UILabel!

    @IBOutlet weak var nameLbl: UILabel!
   
    @IBOutlet weak var accuracyLbl: UILabel!
    
    @IBOutlet weak var powerLbl: UILabel!
    
    @IBOutlet weak var textView: UITextView!
  
    
    func configyreCell(moves: Moves){
        
        typeLbl.text = moves.typeLbl
        nameLbl.text = moves.nameLbl
        accuracyLbl.text = moves.accuracyLbl
        powerLbl.text = moves.powerLbl
        textView.text = moves.textView
        
    }


}
