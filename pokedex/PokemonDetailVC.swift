//
//  PokemonDetailVC.swift
//  pokedex
//
//  Created by Jigar Panchal on 3/15/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import UIKit
import Alamofire

class PokemonDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var pokemon:Pokemon!
    var moves:Moves!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var segment1View: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var defenseLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var pokedexIdLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var attackLbl: UILabel!
    @IBOutlet weak var evoLbl: UILabel!
    @IBOutlet weak var currentEvoLbl: UIImageView!
    @IBOutlet weak var nextEvoLbl: UIImageView!
 
    private var _pokemonUrl:String!
    var movesArr = [Moves]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        nameLabel.text = pokemon.name.capitalized
        
        
       loadInitialView()
  
    }
    
    func loadInitialView(){
        let img = UIImage(named: "\(pokemon.pokedexId)")
        
        
        
        mainImg.image = img
        currentEvoLbl.image = img
        pokedexIdLbl.text = "\(pokemon.pokedexId)"
        
        pokemon.downloadPokemonDetail{
            self.downloadMoveData {
            self.updateUI()
            }
        }
    }
    
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
        if segment.selectedSegmentIndex == 0{

            segment1View.isHidden = true

            loadInitialView()
            
        } else if segment.selectedSegmentIndex == 1{
            
            segment1View.isHidden = false
            
        }
        
    }
    
    
    func updateUI(){
        attackLbl.text = pokemon.attack
        defenseLbl.text = pokemon.defense
        heightLbl.text = pokemon.height
        weightLbl.text = pokemon.weight
        typeLbl.text = pokemon.type
        descriptionLbl.text = pokemon.description
        
        if pokemon.nextEvolutionId == "" {
            
            evoLbl.text = "No Evolutions"
            nextEvoLbl.isHidden = true
            
        } else {
            
            nextEvoLbl.isHidden = false
            nextEvoLbl.image = UIImage(named: pokemon.nextEvolutionId)
            let str = "Next Evolution: \(pokemon.nextEvolutionName) - LVL \(pokemon.nextEvolutionLevel)"
            evoLbl.text = str
        }
        
    }
    
    func downloadMoveData(completed: @escaping DownloadComplete){
        
         _pokemonUrl = "\(URL_BASE)\(URL_POKEMON)\(pokemon.pokedexId)/"
        
        Alamofire.request(_pokemonUrl).responseJSON { (response) in
            
            if let dict = response.result.value as? Dictionary<String,AnyObject>{
  //          print(response)
                if let moves = dict["moves"] as? [Dictionary<String,AnyObject>]{
                print(moves)
                    for obj in moves{
                        let move = Moves(movesDict: obj as Dictionary<String, AnyObject>)
                        self.movesArr.append(move)
                    }
                    self.tableView.reloadData()
                }
            }
            print(self.movesArr.count)
            completed()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MoveCell", for: indexPath) as? MoveCell{
            
            let moves = movesArr[indexPath.row]
            cell.configyreCell(moves: moves)
            
            return cell
        }else{
            return MoveCell()
        }
    }

    @IBAction func backBtnPressed(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        
    }
   
}
