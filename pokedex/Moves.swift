//
//  Moves.swift
//  pokedex
//
//  Created by Jigar Panchal on 3/22/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import Foundation
import Alamofire

class Moves{

private var _typeLbl:String!
private var _nameLbl:String!
private var _accuracyLbl:String!
private var _powerLbl:String!
private var _textView:String!
private var _pokedexId:Int!
private var _pokemonUrl:String!

    var typeLbl:String{
        if _typeLbl == nil{
            _typeLbl = ""
        }
        return _typeLbl
    }
    var nameLbl:String{
        if _nameLbl == nil{
            _nameLbl = ""
        }
        return _nameLbl
    }
    var accuracyLbl:String{
        if _accuracyLbl == nil{
            _accuracyLbl = ""
        }
        return _accuracyLbl
    }
    var powerLbl:String{
        if _powerLbl == nil{
            _powerLbl = ""
        }
        return _powerLbl
    }
    var textView:String{
        if _textView == nil{
            _textView = ""
        }
        return _textView
    }
    var pokedexId:Int{
        return _pokedexId
    }
    init(pokedexId:Int){
        self._pokedexId = pokedexId
         self._pokemonUrl = "\(URL_BASE)\(URL_POKEMON)\(self.pokedexId)/"
    }
    
    init(movesDict: Dictionary<String,AnyObject>) {
        if let type = movesDict["learn_type"] as? String{
            self._typeLbl = type
            
        }
        if let name = movesDict["name"] as? String{
            self._nameLbl = name
        }
        
        if let url = movesDict["resource_uri"] as? String{
            let moveUrl = "\(URL_BASE)\(url)"
            
            Alamofire.request(moveUrl).responseJSON(completionHandler:{(response) in
                if let moveDict = response.result.value as? Dictionary<String,AnyObject>{
                   
                    if let accuracy = moveDict["accuracy"] as? Int{
                        self._accuracyLbl = "\(accuracy)"
                    }
                    
                    if let power = moveDict["power"] as? Int{
                        self._powerLbl = "\(power)"
                    }
                    
                    if let descrition = moveDict["description"] as? String{
                        self._textView = descrition
                    }
                }
             //   completed()
                
            })
            
        }
    }
    


}
