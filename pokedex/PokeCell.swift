//
//  PokeCell.swift
//  pokedex
//
//  Created by Jigar Panchal on 3/15/17.
//  Copyright © 2017 Jigar Panchal. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
    
    @IBOutlet weak var thunbImg: UIImageView!
    
    @IBOutlet weak var nameLbl: UILabel!
   
    var pokemon:Pokemon!
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        layer.cornerRadius = 5.0
    }
    
    func configureCell(_ pokemon : Pokemon){
        self.pokemon = pokemon
        nameLbl.text = self.pokemon.name.capitalized
        thunbImg.image = UIImage(named: "\(self.pokemon.pokedexId)")
    }
}
